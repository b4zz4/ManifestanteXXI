# Manifestante DIY

![Póster](poster/poster_revolucionario_de_accion_chico.png)

* [Comprar versión impresa](https://4232.cf/es/shop/manifestant-diy-poster/)
* [Descargar Poster para imprimir](poster/poster.png)
* [Descargar EPUB](ManifestanteDIY.epub)

> * Casco y protección para la bici para protegerse de la represión
> * Pañuelo en la cara, pasamontaña o [mascara](#mascara): contra el gas lacrimógeno y el [reconocimiento facial](#reconocimiento-facial)
> * [Guante de cuero curtido](#guante-de-cuero-curtido), para sostener las bombas de gas lacrimógeno
> * Ropa de la [gratisferia](textos/Gratiferia.md) para que no te identifiquen con ella y taparte la cara
> * [Edison Carter](#edison-carter): cámara grabando y transmitiendo en vivo
> * Teléfono con [ManifestDroid](#manifestdroid): comunicación criptográfica (SMS, llamadas, chat, etc), publicar fotos sin exponer a los manifestantes, etc.
> * Botella con vinagre, jugo de limón o [neutralizante](#neutralizante): para protegerse del gas lacrimógeno
> * [Mochila con router WIFI](#mochila-wifi) para mejorar el acceso a la red
> * Antenas para transmitir con mayor potencia
> * [Linterna estroboscópica](#linterna-estroboscopica) para calmar a las fieras
> * Protección para [identificaciones con RFID](#proteccin-para-documentos-con-rfid)

## Reconocimiento facial

En el 2011 Cristina Fernández de Kirchner emitió un Decreto ordenando la creación del Sistema Federal de Identificación Biométrica ([SIBIOS](textos/SIBIOS.md)), un servicio de identificación biométrica centralizado, con cobertura nacional, que permitirá a las agencias de seguridad hacer "referencias cruzadas" de información con datos biométricos, con la posibilidad de vincular el sistema de registro facial obtenido a través de las cámaras de vigilancia en la vía pública con las imágenes obtenidas a través de sistema SIBIOS.

El reconocimiento de rostros es cada más simple y barato, por ejemplo en la Ciudad de Buenos Aires las [cámaras de seguridad](http://camaras.buenosaires.gob.ar/) superan las mil y las redes sociales con reconocimiento facial: facebook, google+, flickr, etc, son cada vez más populares y la actualización permanentemente por parte de sus usuarios mejora las bases de datos constantemente.

![Aerosol a Distancia](planos/aerosol_a_distancia.png)
> Se pueden usar caño de luz de 5/8, estos son populares y se los pueden unir este si con 2 trampos si alcanza 7mts de altura

En Android podemos usar el programas [ObscuraCam](https://guardianproject.info/apps/obscuracam/) que oculta las caras de forma automática.

### ¿Que podemos hacer?

* Bloquear o deshabilitar las cámaras de seguridad
* No etiquetar fotos en redes sociales
* Solo compartir fotos de manifestaciones con rostro borrados
* Borrar datos de localización y metadatos de las fotos. 

## Volantes y Poster

Un volante tiene que ser fácil de leer, expresar fácilmente una problemática, proponer una acción concreta.
Tiene que decir, donde, cuando y como nos tenemos que manifestar o que es lo que se necesita hacer.
Usar valores de alto contraste (blanco y negro) para soportar fotocopias.

![Diseño de volante](planos/poster.png "Como diseñar un volante")

> Para diseñarlo podemos usar [Inkscape](http://inkscape.org). Y el formato que usemos tiene que servir para hacer postres y volantes.
> Para diseñarlo lo mejor es usar **SVG** y para imprimirlo **PDF** o **JPG** como mínimo 5000px de ancho o alto.

### Errores comunes

- Ser demasiado extenso
- Contar detalles innecesarios o muy puntuales
- Poner imágenes de fondo
- NO proponer acciones, lo cual deja al lector imposibilitado a participar
- Usar grises y al fotocopiarlo se vea feo 
- Un volante NO es un diario
- Que el formato no soporte impresiones a varios tamaños.

### Engrudo

En una olla disolver la harina en un poco de agua, hasta que no haya grumos y luego añadir la otra revolviendo para integrar ambos 
ingredientes. Llevar la preparación a fuego suave y revolver con cuchara de madera. Continuar revolviendo hasta que comience a hervir y 
en ese momento, retirar del fuego. Dejar enfriar. También se le puede agregar un poco de sodacaustica que mejora la efectividad del pegamento.

_**Nota:** puede hacerse sin cocinar._

#### Ingredientes

* 100 gramos de harina
* 1 litro de agua fría

## Protección para documentos con RFID

Los sistemas [SUBE](textos/SUBE.md), [AFA plus](textos/AFA.md) y los nuevos DNI dan la posibilidad identificar a personas a distancia. La tarjeta SUBE genera un registro de todos los viajes que realiza, violando el derecho a la privacidad, y los guarda en una base de datos controlada por la Secretaría de Transporte. En algunos países ya se a usado este sistema para identificar manifestantes, ya que los lectores de RFID pueden ser leídos a la distancia, como puede verse en el documental "[La Police Totale](http://websuterfuge.free.fr/rfid.html)"

![Estuche anti-RFID](planos/porta_sube.png)

> Estuche **anti-RFID**, armado con un tetra pak, bloquea las ondas de radio así no es detectable a larga distancia. En el caso de la tarjeta SUBE para utilizarla hay que sacarla de su estuche. 
> Aunque los datos de **Mifare 1K** no pueden ser leídos a gran distancia, si se puede **leer el ID** o sea el número de identificación de las tarjetas y DNI.

### ¿Que podemos hacer?

* No usarlos
* No renovar el DNI
* Intercambiar tarjetas de Transporte
* Usar el estuche

## Gas lacrimógeno

De verse afectado de forma real o potencial por este tipo de armas, es necesario conocer las medidas utilizadas para mantener el control de la situación.
También hay que tomar los camiones "hidrantes" como gases lacrimonenos por que tienen neurotoxinas que producen anciedad o panico.

### Mascara

![mascara de contra gas lacrimógeno](planos/mascara.png)

> Corte la **botella de 2 litros** con la forma de su rostro y colocar los **elásticos** de manera que la botella quede firme a la cabeza.
> Colocar **cinta aisladora**, en los bordes, de modo de evitar que entre gas en la máscara, sino puede lastimar los ojos.
> Encastre en la base **un barbijo** o trapo humedecido con vinagre, jugo de limón o **neutralizante**

### Guante de cuero curtido

Generalmente, la "bomba" se puede agarra y lanzar a otra dirección, aunque hay que protegerse las manos con un **paño húmedo grueso** o **guantes** de protección, ya que la bomba se calienta hasta alcanzar altas temperaturas, además, hay que considerar que, quien la tome se verá rodeado de gas momentáneamente.

### Alejar el gas

Podemos llevar un palo o **raqueta de tenis** para alejar lata gas lacrimógeno.

### Neutralizante

![neutralizante](planos/lacrimogeno_neutralizante.png)

> Prepare un neutralizador en **un roseador** con agua potable con 5% de **bicarbonato de sodio**.
> El roseador no tiene que ser mayor que 500cm3 (medio litro) por que se vuelve pesado, difícil de manejar o llevar colgado.

#### ¿Que podemos hacer?

- Rosearse neutralizante en ojos y boca, sin frotar.
- Protegerse la nariz y la boca con un trapo empapado de vinagre, neutralizante o jugo de limón.
- Ponerse un pedazo de limón en la boca.
- No llevar lentes de contacto o [usar protección](#mascara).
- Evitar que el gas alcance los ojos, para ello se pueden llevar preparadas gafas de buceo, una [mascara](#mascara) o en todo caso cerrarlos y llevarlos hacia el suelo, ya que el gas se disipa hacia arriba.

## Linterna estroboscópica

Estas luces se usa para control mental, inducir al sueño o poner en estado de trance en las velocidades adecuadas producen estado de calma, somnolencia o la sensación de falta de tiempo. 
En las frecuencias entre _5hz_ a _7hz_ produce un estado de calma, eso es lo que intenta generar el este circuito. Podemos modificar una linterna que use entre 5v a 12v con este circuito, y reutilizar su lamparas LED y baterías.

![circuito](planos/555_Estroboscopica.png)

> Uno o más LED, circuito integrado 555, resistencias de 47k, 82k y 1k, capacitor electrolítico 1mf, capacitor cerámico 1mf, pilas.
> _Estos componentes se pueden comprar en cualquier casa de electrónica_

_**Nota:** Si usas una mascara anti gas lacrimogeno de color verde podes poner un filtro a la lampara para que no te afecte._

Además podes mejorar el efecto con sonidos binaural amplificado con un megáfono o amplificador. Se puede reproducirlo "debajo" de música o cuando alguien habla en los posible este sonido tiene que ser escuchado inconscientemente.
Hay un generador para Android llamado **binauralbeat**

## Láser de alta potencia

![láser de alta potencia](planos/laser.png)

Estos se pueden usar para molestar a la visión de los policías (en Brasil) y en Turquía casi derriban un helicóptero de monitoreo. Así como para generar visualización de la manifestación.
Tambien se pueden usar luses infrarojas o led de alta intensidad para bloquear las cámaras de los policias que suelen grabar las manifestaciones.

## ManifestDroid

Tanto en las computadoras como los dispositivos portátiles (celulares, pad, tablet, etc) estamos muy mal acostumbrados a comunicarnos de modo inseguro, conectándonos a cualquier red disponible, comunicarnos y navegar sin criptografía, pudiendo cualquier persona cercana a nosotros interceptar lo que estamos haciendo, además de la compañía de celular o internet pueden hacerlo todo el tiempo.

* [Guardian Project](https://guardianproject.info/apps/)

_Hay que tener en cuenta que cuantos más seamos usando estos recursos (cifrado, proxys, túneles, etc) **todo el tiempo**, no solo para hacktivismos, **más seguro** se tornan, por que generamos tráfico cifrado todo el tiempo y más complicado se vuelve encontrar los grupos de activistas._

_**Consejo:** No uses un celular o dispositivos portátiles para nada. [Articuló](https://rosenzweig.io/blog/no-cellphones.es.html) muy interesante._

### Red libre con Android

Muchos dispositivos con Android no son celulares, así que no tienen acceso a internet permanente y en el caso de los celulares internet puede ser interrumpida, limitada o bloqueada, por eso es preferible armar nuestra propia red así podremos chatear con Bounjour, compartir archivos y conectarnos con diversos equipos. Además muchos dispositivos permite compartir internet dentro de la misma red libre usando el 3G.

## Mochila WIFI

Es un router dentro de una mochila (bolso, cartera, etc) con baterías para sostener su funcionamiento por horas.
Sin ninguna configuración y con una red sin clave sirve para armar una [red local](#red-local).

Con algo de configuración podemos usarlo para compartir internet usando Tor o difundir un [portal cautivo](https://es.wikipedia.org/wiki/Portal_cautivo) que solo transmitr una webcam o radio, esto es muy util para acampes publicos para consientizar a los vecinos.

![mochila WIFI](img/media/PIC_0041.jpg)

_Por ahora falta la documentación, podes leer la experiencia del [Nodo de Guerrilla](//b4zz4/NodoDeGuerrilla/) y el 'satélite' hogareño [Mogul](//b4zz4/mogul/)._

### Edison Carter

Cámara para transmitir lo que ocurren en el momento en vivo y en directo, a la vez sirve para grabar y publicar.

![cámara WIFI con el panel solar](img/media/PIC_0175.jpg)  ![cámara wifi con panel solar](img/media/PIC_0176.jpg)

Esta basada en un router WIFI, una webcam. Puede alimentarse con pilas, baterías y panel solar. 
Como esta pensada para salir a la calle y transmitir del modo más económico, usa las redes disponibles, transmite en baja resolución, en formatos muy simples (JPG, [MJPEG](https://es.wikipedia.org/wiki/MJPEG)), y siempre esta disponible en un portal cautivo.
A la vez al ser un dispositivo totalmente con software libre es más confiable que un telefono movíl.

# Radio con una raspberry

Se puede armar una radio con un alcance de 100mts con una Raspberry. Se puede armar un amplificador de señal con algo de cocimiento técnico.
Podemos usar este transmisor para transmitir una radio abierta de modo que los vecinos puedan escucharla en el barrio.

![radio raspberry](planos/radio_raspberry.png)

Existe [rpitx](https://github.com/F5OEO/rpitx/) que permite transmitir en LF, VHF y UHF. En una banda angosta (NFM), un ancho de banda más acotado que la FM tradicional.
Tambien podemos transmitir en AM pero es un poco más complicado.

Con este programa se pueden crear interferencias (jammer) en las ondas de GPS, GSM u otras señales esto puede usarse preventivamente si tenemos un aparto espiá que desconocemos, ejemplo:

`sudo pichirp 513e06 4e6 1`
 
En este ejemplo va a interferir de 513mhz hasta el 517mhz tardando un segundo.

`ffmpeg -i http://archivo-o-cast.mp3 -f s16le -ar 44.1k -ac 1 - | csdr convert_i16_f | csdr gain_ff 44000 | csdr convert_f_samplerf 44100 | sudo rpitx -i- -m RF -f "99.5e3"`

En este ejemplo transmite una radio online o archivos en 99.5mhz

## Transmisor direccional desde azotea

Muchas casas tiene un tendedero tipo calesita en la terraza, aunque no podemos hacer una radio de gran alcance omnidireccional (hacia todos lados) podemos transmitir de modo direccional a gran alcance.

![Antena moxon con raspberry](planos/moxon.png)

Sobre una superficie de cartón o plástico poner un alambre de cobre fino, este alambre puede sacarse de cables o transformadores viejos. Necesitamos aproximadamente dos tramos de 65cm y otro de 1,42mts. pegarlo sobre el cartón y unir uno de los lados de 65cm al pin 7 y otro al pin de GND (masa) el tramos más largo de la antena queda libre. Para mayor independencia podemos usar pilas para alimentar el transmisor.

![Antena moxon sobre la calesita](planos/tendedero.png)

En un terraza se puede poner sobre un tendedero y girar un poco cada tantos minutos de modo que el mensaje llegue a la mayo zona posible.
Si no tenemos una calesita podemos hacerlo a mano o dejarla en una sola dirección alejada de la superficie del suelo, por ejemplo sobre una mesa o cajones.

> * La antena propuestas esta al rededor de los 80 a 150mhz
> * Los lugares altos son los mejores para transmitir llegan a todo lo visible
> * Este metodo se puede seguir usando aunque no halla Internet o electricidad

# Logistica DIY

![poster de inteligencia](poster/logistica_chico.png)

> Para soportar toda la actividad en las calles se necesita de varios equipos de inteligencia que coordinen y retransmitan todo lo que ocurre creando medios de comunicación tanto para difundir como para coordinar. 

Al igual que los policías, puede coordinarse la inteligencia por fuera de la manifestación, las personas calmadas piensan mejor, en este caso el grupo de acción debe pasar reportes precisos de lo que ocurre. La mayoría de la comunicación (a nivel inteligencia) debe hacerse por chat, en medio de una manifestación hablar se tornarse complicado e inseguro. 

Sobre todos necesitan tener **GNU/Linux** hay varias versiones y cada una sirve para las diferentes actividades que el equipo de coordinación, difusión, organización necesitan. 
Si no probaste ninguna podes generar tu propio **pendrive vivo** con el que podrás iniciar tu computadora usar gnu/linux y luego instalarlo.

> * [Sistema operativo Tails](https://tails.boum.org/) pensado para manifestantes y activistas.
> * [Navegación anonima Tor](#navegar-con-tor), tuneles para anonimizar la navegación
> * Difusión con bots para redes sociales
> * Transmitir [televisión](#transmisión-de-tv) y [radios](#transmisión-de-radio)
> * Pidgin para chatear en [IRC](#IRC), [Jabber](#Jabber), [Tox](#Tox) y/o usar Tor para anonimizar
> * Servidor hogareño hecho con viejas maquinas o pequeñas como raspberry
> * Umap para organizar la accion en la calles
> * espeak para dar comunicados sin ser descubiertos.
> * Quitar metadatos a los archivos
> * Eliminar sin dejar rastros.

## Comunicación segura y coordinación

Una de las grandes ventajas de los policías en una manifestación, es que se encuentran organizados, pueden comunicarse en tiempo real, modificar sus movimientos y planificar en base a lo ocurrido. Para esto cuentan con un grupo dedicado a esta función que esta fuera y dentro de la acción.

Para organizarse **internamente** necesitamos no generar demasiado trafico rastreable en internet.
O sea por cualquier cosa que puede ser guardada por servidor, proveedor de internet o el de mail, red social, IRC.
Así que una de las mejores cosas que podemos hacer es tener una VPN, red de pares y/o método de publicación anónimo o usar conexiones cifradas.

Nada que pueda ser comunicado en una **red local** debe pasar por una internet. 
En una red local podemos chatear usando **Bounjour** para chatear entre todos los disponibles en la misma red. 
Aun así debemos usar [OTR](textos/Pidgin_con_OTR.md).

### Lista de mails

Las listas de correo son excelentes para organizarse en grupos sin la necesidad de estar todo el día conectado.
Para ser más efectivos lo más recomendables es respetar las [Netiquetas](textos/Netiquetas.md) 

Vale la pena indicar que los mails son descentralizados por ende podemos tener la lista en [Riseup](https://lists.riseup.net/www/) o 
[Autistici](http://www.autistici.org/es/services/lists.html) y asociarlas a cuentas de mail en otros servidores.

### Chat

<!-- tox -->

Lo mismo podemos hacer los manifestantes, organizándonos con **herramientas similares**, **libres** y **seguras**. Lo mejor es que un grupo en particular se dedique a la coordinación, utilizando con Bounjour, IRC, Jabber o telefonía sobre internet. 

La **criptografía** es como si inventáramos nuestro propio idioma para hablar con una persona, aun así para nosotros sigue siendo totalmente legible.
Esto sirve para que nadie pueda interceptar la conversación. El modo de ciptografía más practico y popular en el Chat es el **OTR**.

Los servidores de **IRC** son servidores de chat con muchas salas tematicas (canales).
No necesitan cuenta, pueden usarse muchos servidores; descentralizando la conversación.
Si queres probar el **IRC** podes probar [Webchat](https://webchat.freenode.net/) de Freenode.
Es tan simple como ponerte un **nick** y un **canal** (sala de chat) donde hablar.

El hecho de que sea una servicio descentralizado nos da la posibilidad de tener una cuenta en diferente servidor que nuestro contacto.

Dentro de una manifestación puede usarse un chat local por bluetooth

#### Consejos

* Usar [tox](https://tox.chat/) que es P2P.
* Crear tu cuenta de jabber en [Riseup](https://riseup.net/es), [Autistici](http://www.autistici.org/es) y/o [Jabber.es](http://jabber.es/)
* Usar simpre [OTR](textos/Pidgin_con_OTR.md)
* Conectarnos usando **Tor**
* Tener varias cuentas y diferentes protocolos

## Navegar con Tor

![Tor Browser Bundel](img/torbrowser.png)

Es un modo de navegación anonima. Este no debe usarse para cuentas personales, si no para la navegación convencional, buscar datos, visitar blog, etc.
La versión para Android se llama **Orbot**, nos permite navegar con **OrWeb** y al igual que en nuestra computadora podemos usar el Tor para anonimizar nuestra navegación como chats y otros servicios dentro de internet.

> El navegador [Tor Browser](https://www.torproject.org/projects/torbrowser.html.en) debe descargarse de la pagina oficial.

### Consejos

* [Bloquear publicidades](https://prism-break.org/es/all/#web-browser-addons)
* Bloquear [script inseguros](https://noscript.net/)
* Usar Lector de noticias
* NO usar cuentas sincronizadas con nuestras cuentas de email o número de celular
* NO usar las mismas cuentas en Tor y sin Tor.

## Lector de Noticias

Muchas paginas, blogs, centrales de noticias tiene ![RSS](img/rss.png) RSS o Atom con la que podemos mantenernos actualizados de las novedades, noticas, o eventos y actividades de colegas y amigos.

![Brief](img/lector_de_noticias.png)

> Los lectores de noticias son un modo practivo y no rastreable (usando **Tor**) de mantenernos informados y actualizados.
> Podes usar [Brief](http://brief.mozdev.org/), es un complemento de Firefox podemos instarlo en Tor Browser, anonimizando nuestras fuentes de noticias.

## Difusión

### Servidores (sobre tor)

<!-- imagen a futuro -->

> Para pasar documentos o publicar de modo anónimo, descentralizado y desde tu propia computadora.

* [Guía como levantar un servidor](//b4zz4/mini-ciboulette)

#### Consejos

* Usar [tor2web](https://tor2web.org/) para difundir en internet.
* Estos programas pueden correr sobre una raspberry o router así puede estar conectado todo el tiempo.
* Borrar la metadata de la información que te rastree.
* Con [shallot](//katmagic/Shallot) podes ponerle un nombre bonito al .onion

### Transmisión de Radio

Para poder transmitir radio en internet necesitamos un **punto de montaje** en un servidor Icecast, una especie de antena de transmisión en internet. 
Podemos conseguir uno en [giss.tv](http://giss.tv/addmount.html) y luego instalar [Mixxx](http://mixxx.org/)

![Vista general del programa](img/mixxx.png "Vista general del programa")
> **Mixxx** es una herramienta poderosa, flexible y dinámica que permite crear programación radial vía Internet.
> [IDJC](http://idjc.sourceforge.net/) similar a la anterior.
> En esta pantalla se puede crea la secuencia musical que se utilizará en el programa de radio, activar y desactivar el micrófono.

### Transmisión de TV

Podemos transmitir nuestro propio canal usando [OBS](http://obsproject.com/)

### Mensajes al pùblico

![Interface de Gespeak](img/gespeak.png "Interface de Gespeak")

Anonymous hace tiempo que usa sintetizadores de voz para difundir sus noticias y campañas.
Esto es muy bueno por que no es posible reconocer quien habla, a la vez se puede programar mensajes automáticos para enviar mensajes al publico.

### PyVoiceChanger

![Interface de PyVoiceChanger](https://raw.githubusercontent.com/juancarlospaco/pyvoicechanger/master/temp.jpg)

[PyVoiceChanger](//juancarlospaco/pyvoicechanger#pyvoicechanger "PyVoiceChanger el repositorio oficial") es un deformador de voz en tiempo real escrito en un solo archivo, fácil de usar.

## Proteger información

### Cifrados de una carpeta

Cifrar una carpeta con todos su contenido de manera ocultar a cualquier persona que acceda físicamente a nuestra computadora o dispositivo movíl.
	
![Cryptkeeper](img/cryptkeeper.png "Cryptkeeper en el menú de notificaciones")
> Crptkeeper en el menú de notificaciones.


### Bufanda Rosa

Este programa permite **cifrar** y **descifrar** mensaje, archivos para enviarlos por correo electronico, publicarlo, guardar información de cualquier tipo dentro de una imagen o audio.
La información puede estar disponible de modo público y sin la clave correspondiente nunca nadie podrá acceder a los datos ocultos.

![Pantalla principal de Bufanda Rosa](img/bufandarosa.png "Cifrado y Descifrado")
> Puede resultar útil si pretendemos enviar una información confidencial a una persona y no queremos que la información sea interceptada por otros.

[Descargar](//b4zz4/BufandaRosa)

#### Consejos

* **NUNCA** envies las claves junto al archivo o mensaje cifrado
  * Compartirlas en vivo (de persona a persona)
  * En un [chat con OTR](textos/Pidgin_con_OTR.md)
* Cambia tus claves periodicamente
* Borrar los **metadatos** de fotos de cámaras y celulares antes de publicarlas en noticias, redes sociales, blogs.
* Hay programas para android en [f-droid](https://f-droid.org/)

## Textos

Para difundir textos, usar texto "plano" sin formato, en TXT o Markdown. Estos son muy fáciles de interpretar, reutilizar.
Que no pueden contener metadatos y no es nada fácil analizar el origen. 

### Consejos

* NUNCA usar word ¡¡NUNCA!!
* en lo posible NO usar PDF tiene metadatos
* Si usamos PDF, que sean realizados con software libre.

## Protestas

### LOIC

Es una aplicación popularizada por **Anonymous** que se encarga de realizar ataques de denegación de servicios a páginas web **DDoS**.
La técnica es vieja pero puede servir para visualizar una protesta, en la gran mayoría de los servidores no funciona.

### Mapas

![Umap](img/umap.png)
> [Umap](https://umap.openstreetmap.fr) mapas para coordinarse en las calles.

_**Nota:** es preferible usar capturas de pantalla y guardarlo en tu servidor sobre tor._

# Notas relacionadas

Algunas experiencias relacionadas.

* Experiencias de los [movimientos anti-globalizadores](https://es.wikipedia.org/wiki/Contracumbre_del_G8_en_G%C3%A9nova) en 2001 como el "Tute Bianchi"
* Mapa con el [movimientos de la Policía en Estambul](http://www.microsiervos.com/archivo/tecnologia/manifestantes-turquia-controlar-policia.html), como los manifestantes turcos vigilan a la policía.
* [Cómo filmar una manifestación](http://media.lavaca.org/pdf/mu/mu73.pdf) Excelente nota de la revista Mu.
* Los manifestantes hacen [huir a la policia](https://www.youtube.com/watch?v=H42UJIszoaQ) en Brasil
* Los manifestantes españoles tiene un [helicoptero](https://www.youtube.com/watch?v=h-oO8I5vR5s) para ver lo que hace la policía.
* [Prism Break](http://prism.hackcoop.com.ar/#es) - Lucha contra la NSA.
* [Tutoriales](https://guardianproject.info/apps/tutorials/) de software libre para Android. (en ingles)
* [Ataque laser contra helicopteros](http://www.biobiochile.cl/2013/06/30/manifestantes-realizan-impresionante-ataque-laser-contra-helicoptero-militar-en-egipto.shtml)
* [MidiNinja](https://knightcenter.utexas.edu/es/blog/00-14203-midia-ninja-un-fenomeno-del-periodismo-alternativo-que-emergio-de-las-protestas-en-bra) colectivo que transmite usando 4G directamente desde las calles 
* [CamOver](http://noticias.lainformacion.com/mundo/camover-el-juego-que-pretende-acabar-a-golpes-con-todas-las-camaras-de-videovigilancia-de-berlin_8z9GyH5P4kGbnLAcjE1vC/) El juego para destruir las camaras de seguridad de Berlín.
* [Tech Tools For Activism](https://techtoolsforactivism.org/) Herramientas digitales para activistas (en ingles)

# Licencia

Se permite el uso comercial de la obra y de las posibles obras derivadas, la distribución de las cuales se debe hacer con una licencia igual a la que regula la obra original.

![GFDL](img/128px-GFDL_Logo.svg.png)
![Creado para la comunidad compartir con la misma licencia](img/by-sa-125px.png)

# Donaciones ❤

* **Bitcoin:** 19qkh5dNVxL58o5hh6hLsK64PwEtEXVHXs
